module gitlab.com/telecom-tower/sdk

require (
	github.com/pkg/errors v0.8.0
	gitlab.com/telecom-tower/towerapi v1.0.1
	golang.org/x/sys v0.0.0-20181011152604-fa43e7bc11ba // indirect
	google.golang.org/genproto v0.0.0-20181004005441-af9cb2a35e7f // indirect
	google.golang.org/grpc v1.15.0
)
